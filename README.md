# Proof of Concept for a Custom X Window System Desktop

This is a quick and dirty proof of concept (POC) to see how a custom
[X Window System](https://www.x.org/) Desktop could be done.

## Demo

When properly provisioning and booting virtual
machine (VM) provided in this POC ([see Development/Test
Environment](#developmenttest-environment)) you shall select `POC Desktop`
as the desktop you wish to use.

![Login manager with desktop selection offering POC Desktop](./assets/login-manager.png)

Once logged in ([see default credentials](#default-credentials)), you
shall see a very different desktop than the default Gnome3 Desktop also
available in the VM. Switch back and forth if you don't believe it! ;)

![POC Desktop Screenshot in all it's beauty](./assets/poc-desktop.png)

## How it Works

For the [X Window System](https://www.x.org/) to be _aware_ that your
desktop exists, you need two things:

1. A [Desktop Entry](./src/poc-desktop/poc.desktop)
inside `/usr/share/xsessions/` ([see Desktop Entry
Specification](https://specifications.freedesktop.org/desktop-entry-spec/desktop-entry-spec-latest.html))
2. A [Shell Script](./src/poc-desktop/poc-desktop) that will be run
when your X session starts
    * The shell script is a simple script starting a bunch of programs
    in background, in the proper order.
    * Last program launched by the shell script shall be the window
    manager (in foreground, not background)

## Components of a Desktop

### Window Manager

> A window manager is system software that controls the placement and
appearance of windows within a windowing system in a graphical user
interface.

https://en.wikipedia.org/wiki/Window_manager

For this POC, the [Openbox](http://www.openbox.org/) window manager has
been chosen.

### Panels & Toolbars

> A panel is "a particular arrangement of information grouped together
for presentation to users in a window or pop-up".

https://en.wikipedia.org/wiki/Panel_(computer_software)

> A taskbar is an element of a graphical user interface which has various
purposes. It typically shows which programs are currently running.

https://en.wikipedia.org/wiki/Taskbar

For this POC, the [Cairo-Dock](https://glx-dock.org/) panel has been
chosen.

### Composite Manager

> A compositing window manager, or compositor, is a window manager that
provides applications with an off-screen buffer for each window. The
window manager composites the window buffers into an image representing
the screen and writes the result into the display memory.

> Compositing window managers may perform additional processing on
buffered windows, applying 2D and 3D animated effects such as blending,
fading, scaling, rotation, duplication, bending and contortion,
shuffling, blurring, redirecting applications, and translating windows
into one of a number of displays and virtual desktops. Computer graphics
technology allows for visual effects to be rendered in real time such
as drop shadows, live previews, and complex animation. Since the screen
is double buffered, it does not flicker during updates.

https://en.wikipedia.org/wiki/Compositing_window_manager

For this POC, the [Compton](https://github.com/chjj/compton/) composite manager
has been chosen.

## Development/Test Environment

The development/test environment (dev. env.) is contained inside a
VM. Everything will happen in VM for this POC.

### Too Long ; Didn't read (tl;dr)

1. Provision the VM (`vagrant up --provision && vagrant halt`)
2. Start the VM (`vagrant up`)

### Setup the Development/Test Environment

A [Vagrantfile](./Vagrantfile) is made available that brings up a
[VirtualBox](https://www.virtualbox.org/) VM based on [Debian 9 Stretch
64bits](https://www.debian.org/).

It uses a the [official public box
debian/stretch64](https://app.vagrantup.com/debian/boxes/stretch64)
which is a vanilla no-desktop setup.

Provisioning makes sure that the VM is configured as a _desktop machine_.

#### Using Vagrant

Vagrant has been used to ease VM manipulation.

Following instructions assumes you have a working [Vagrant
installation](https://www.vagrantup.com/downloads) on your system.

#### Vagrant Configuration

Again, for simplicity, you shall install the VirtualBox guest additions plugin:

```
vagrant plugin install vagrant-vbguest
```

### Provision the Development/Test Environment

You are now ready to provision the VM to make sure the VM is
properly configured as a _desktop machine_.

Since we wanted to use an official box and didn't want to create a
[Vagrant Box](https://app.vagrantup.com/boxes/search) for a POC, we need
to start the VM, provision a default desktop environment and halt the
VM so next time it fires up: the machine will boot as a _desktop machine_.

```
vagrant up --provision
vagrant halt
```

### Start the Development/Test Environment

Now that provisioning is done and VM has is now configured as a _desktop machine_, you can boot it.

```
vagrant up
```

# Default Credentials

Use `vagrant` for username __and__ password.

# Credits

* Lizenz Creative Commons BY NC SA.png : Image found on https://wallpapersafari.com/w/lcEBIr
